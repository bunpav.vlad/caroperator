<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Feedbacks extends ActiveRecord
{
	public $captcha;

	public function rules()
	{
		return [
			[['name', 'email', 'text', 'captcha'], 'required', 'message' => \Yii::t('app', 'This field is required')],
			['email', 'email', 'message' => \Yii::t('app', 'Invalid email')],
			['captcha', 'captcha', 'message' => \Yii::t('app', 'Incorrect Code')],
			['text', 'validateTags']
		];
	}

	public function validateTags($attribute, $params)
    {
    	$tags = '';
        $cutTags = strip_tags($this->$attribute, $tags);
        if (mb_strlen($this->$attribute) !== mb_strlen($cutTags))
        {
        	$this->addError($attribute, \Yii::t('app', 'You have used forbidden tags. Whitelist: {tags}.', ['tags' => 'not using tag']));
        }
    }


	public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert))
        {
            $this->ip = Yii::$app->getRequest()->getUserIP();
            $this->agent = Yii::$app->getRequest()->getUserAgent();
	        $this->date = new Expression('NOW()');
	        return true;
	    }
        else
        {
	        return false;
	    }
	}

}

?>