<?php
return [
    'Add feedback' => 'Добавить запись',
    'Date' => 'Дата',
    'Feedback' => 'Запись',
    'Feedbacks' => 'Записи',
    'Form errors. {open_link}Back to form.{close_link}' => 'Форма заполнена с ошибками. {open_link}Перейти к фарме.{close_link}',
    'Guestbook' => 'Гостевая книга',
    'Incorrect Code' => 'Неверно введён код',
    'Invalid email' => 'Некорректный email',
    'Name' => 'Имя',
    'Preview' => 'Перд просмотр',
    'Send' => 'Отправить',
    'Tags:' => 'Теги:',
    'TagsList' => 'Нельзя использовать теги',
    'This field is required' => 'Не заполнено поле',
    'You have used forbidden tags. Whitelist: {tags}.' => 'Вы не можете использовать теги. Список дозволених: {tags}',
    '{name}, your feedback has been sent.' => '{name}, ваша запись отправлена.',
];
